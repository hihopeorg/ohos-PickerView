# PickerView
## 简介
>选择器，包括时间选择、地区选择、分割线设置、文字大小颜色设置

## 效果展示
显示时间选择器:

![](preview/show.png)

时间选择器（农历与阴历的切换）:

![](preview/time.png)

时间（年月日 时分秒）选择器:

![](preview/time_year_second.png)

圆形分割器选择器:

![](preview/circle.png)

城市（省、市）选择器:

![](preview/province.png)

自定义选择器:

![](preview/customize.png)

卡片选择器:

![](preview/card.png)

城市（省、市、区）选择器:

![](preview/provinceToCity.png)
## 下载安装
```shell
npm install @ohos/pickerview --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明
提供多种类型选择器，使用方法类似，以显示时间选择器为例

1. 初始化：实例化scroller和对应的ShowTimePickerComponent.Model 对象
 ```
private scroller: Scroller = new Scroller()
@State showTimeData: ShowTimePickerComponent.Model = new ShowTimePickerComponent.Model();
 ```
2. 属性设置：通过Model类对象设置UI属性来自定义所需风格，也可以添加所需的回调
 ```
 aboutToAppear() {
    this.showTimeData
        .setDividerLineStroke(2)
        .setDividerLineColor(Color.Black)
        .setFontSize(20)
        .setFontColor(Color.Red)
        .setConfirmButtonFont("确定")
        .setCancelButtonFont("取消")
        .setCancelButtonColor(Color.Red)
        .setConfirmButtonColor(Color.Black)
        .setTitleFontSize(20)
        .setTitleFontColor(Color.Black)
        .setPickerSpace(20)
        .setButtonBackgroundColor("#DCDCDC")
        .setYearRangeStart(2001)
        .setYearRangeEnd(2050)
        .setDefaultSelection([2005, 5, 11])
        .setDividerType(DividerType.CIRCLE)
        .setLineSpacingMultiplier(40)
        .withClick((event: ClickEvent) => {
        this.showTimeData.withText("clicked " + this.count++ + " times");
      });
 ```
3. 界面绘制: 调用对象的构造方法，传递已经实例化的对象。
 ```
 build() {
    Stack() {
      Scroll(this.scroller) {
        Column() {
          ShowTimePickerComponent({model: this.showTimeData})
       }
      }
    }
  }
 ```
## 接口说明
`@State showTimeData: ShowTimePickerComponent.Model = new ShowTimePickerComponent.Model();`
1. 设置分割线宽度
   this.showTimeData.setDividerLineStroke(2)
2. 控制分割线颜色
   this.showTimeData.setDividerLineColor(Color.Black)
3. 控制文字大小
   this.showTimeData.setFontSize(20)
4. 控制文字颜色
   this.showTimeData.setFontColor(Color.Red)
5. 控制确定按钮显示的文字
   this.showTimeData.setConfirmButtonFont("确定")
6. 控制取消按钮显示的文字
   this.showTimeData.setCancelButtonFont("取消")
7. 控制取消按钮的文字颜色
   this.showTimeData.setCancelButtonColor(Color.Red)
8. 控制确定文字颜色
   this.showTimeData.setConfirmButtonColor(Color.Black)
9. 设置标题、确定按钮、取消按钮文字大小
   this.showTimeData.setTitleFontSize(20)
10. 设置标题文字颜色
    this.showTimeData.setTitleFontColor(Color.Black)
11. 设置选择器页面间隔
    this.showTimeData.setPickerSpace(20)
12. 设置按钮背景颜色
    this.showTimeData.setButtonBackgroundColor("#7CDCDC")
13. 时间开始范围
    this.showTimeData.setYearRangeStart(2001)
14. 时间结束范围
    this.showTimeData.setYearRangeEnd(2050)
15. 设置分割线类型
    this.showTimeData.setDividerType(DividerType.CIRCLE)
16. 设置间距
    this.showTimeData.setLineSpacingMultiplier(40)
## 约束与限制

在下述版本验证通过：

- DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）。

## 目录结构
````
|---- pickerview 
|     |---- entry  # 示例代码文件夹
|     |---- pickerview  # pickerview库文件夹
|           |---- src
         |----main
            |----ets
               |----components
                  |----common
                     |----AreaDataPickerViewLib.ets #AreaDataParseSample选择器
                     |----CardModel.ets                  #卡片数据生成类
                     |----CardPickerComponent.ets        #卡片选择器
                     |----ChinaDateModel.ets             #时间实体类
                     |----CircleDividerViewLib.ets       #圆形分割器
                     |----CityPickerComponent.ets        #城市选择器
                     |----Constant.ets                   #常量
                     |----CustomizeModel.ets             #自定义实体
                     |----CustomizePickerComponent.ets   #自定义选择器
                     |----HourSecondMinuteComponent.ets  #时分秒选择器
                     |----HourSecondMinuteModel.ets      #时分秒选择器实体
                     |----LunarCalendar.ets              #农历方法
                     |----LunarTimeUtil.ets              #阳历阴历切换
                     |----PickerData.ets                 #选择器分类
                     |----PickerModel.ets                #选择器模型
                     |----Province.ets                   #省、市、区数据
                     |----ShowTimePickerViewLib.ets      #显示时间选择器
                     |----ShowToast.ets                  #提示框
                     |----SolarCalendar.ets              #阳历方法
                     |----TimePickerComponent.ets        #时间选择器
                     |----YearReachSecondComponent.ets   #年到秒时间选择器
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos-PickerView/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/ohos-PickerView/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/ohos-PickerView/blob/master/LICENSE) ，请自由地享受和参与开源。
