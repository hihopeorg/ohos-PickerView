/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DividerType, deviceWidth } from './Constant'
import {
  initializeCustomizePickerDataOnStartup,
  CustomizePickerBean,
  SecondAndThirdArray
} from './CustomizePickerModel'

@CustomDialog
struct CustomizeDialogController {
  controller: CustomDialogController
  cancel: () => void
  confirm: () => void
  scrollerFirst: Scroller = new Scroller()
  scrollerSecond: Scroller = new Scroller()
  scrollerThird: Scroller = new Scroller()
  private lastFirstPosition: number
  private lastSecondPosition: number
  private lastThirdPosition: number
  private buttonHeight: number = 50
  private halfMoveRatio: number = 0.5
  private array = [1, 2, 3]
  @State scrollerFirstIndex: number = 0
  @State scrollerSecondIndex: number = 0
  @State scrollerThirdIndex: number = 0
  @State customizePickerModel: CustomizePickerComponent.Model = new CustomizePickerComponent.Model()

  build() {
    Column() {
      Flex({ justifyContent: FlexAlign.SpaceAround, direction: FlexDirection.Row }) {
        Button(this.customizePickerModel.cancelButtonFont)
          .height(this.buttonHeight)
          .fontSize(this.customizePickerModel.titleFontSize)
          .backgroundColor(this.customizePickerModel.buttonBackgroundColor)
          .fontColor(this.customizePickerModel.cancelButtonColor)
          .margin({ left: 10 })
          .onClick(() => {
            this.controller.close()
            this.cancel()
          })
        Column() {
          Row() {
            Text("自定义选择器")
              .height(this.buttonHeight)
              .fontSize(this.customizePickerModel.titleFontSize)
              .fontColor(this.customizePickerModel.titleFontColor)
          }
        }

        Button(this.customizePickerModel.confirmButtonFont)
          .fontSize(this.customizePickerModel.titleFontSize)
          .height(this.buttonHeight)
          .backgroundColor(this.customizePickerModel.buttonBackgroundColor)
          .fontColor(this.customizePickerModel.confirmButtonColor)
          .onClick(() => {
            this.controller.close()
            this.confirm()
          })
      }.backgroundColor("#7CDCDC").margin({ bottom: 0 })

      Stack({ alignContent: Alignment.TopStart }) {
        if (this.customizePickerModel.dividerType == DividerType.FILL) {
          Divider()
            .vertical(false)
            .strokeWidth(this.customizePickerModel.dividerLineStroke)
            .color(this.customizePickerModel.dividerLineColor)
            .lineCap(LineCapStyle.Round)
            .margin({ left: 10, top: this.customizePickerModel.textHeight * 2, right: 10 })
          Divider()
            .vertical(false)
            .strokeWidth(this.customizePickerModel.dividerLineStroke)
            .color(this.customizePickerModel.dividerLineColor)
            .lineCap(LineCapStyle.Round)
            .margin({ left: 10, top: this.customizePickerModel.textHeight * 3, right: 10 })
        } else if (this.customizePickerModel.dividerType == DividerType.CIRCLE) {
          Row() {
            Ellipse({ width: "30%", height: this.customizePickerModel.textHeight })
              .fillOpacity(0)
              .stroke(this.customizePickerModel.dividerLineColor)
              .strokeWidth(this.customizePickerModel.dividerLineStroke)
              .margin({
                //left: this.customizePickerModel.columnLeftWidth[0],
                top: this.customizePickerModel.textHeight * 2
              })
            Ellipse({ width: "30%", height: this.customizePickerModel.textHeight })
              .fillOpacity(0)
              .stroke(this.customizePickerModel.dividerLineColor)
              .strokeWidth(this.customizePickerModel.dividerLineStroke)
              .margin({
                top: this.customizePickerModel.textHeight * 2
              })
            Ellipse({ width: "30%", height: this.customizePickerModel.textHeight })
              .fillOpacity(0)
              .stroke(this.customizePickerModel.dividerLineColor)
              .strokeWidth(this.customizePickerModel.dividerLineStroke)
              .margin({
                top: this.customizePickerModel.textHeight * 2
              })
          }.width('100%').justifyContent(FlexAlign.SpaceAround)

        } else if (this.customizePickerModel.dividerType == DividerType.WRAP) {
          Row() {
            ForEach(this.array,
              (index: number) => {
                Column() {
                  Divider()
                    .vertical(false)
                    .strokeWidth(this.customizePickerModel.dividerLineStroke)
                    .color(this.customizePickerModel.dividerLineColor)
                    .lineCap(LineCapStyle.Round)
                    .margin({
                      top: this.customizePickerModel.textHeight * 2
                    })
                    .width(this.customizePickerModel.lineWidth)
                  Divider()
                    .vertical(false)
                    .strokeWidth(this.customizePickerModel.dividerLineStroke)
                    .color(this.customizePickerModel.dividerLineColor)
                    .lineCap(LineCapStyle.Round)
                    .margin({
                      top: this.customizePickerModel.textHeight * 1
                    })
                    .width(this.customizePickerModel.lineWidth)
                }
              },
              (index: number) => index.toString())
          }.width('100%').justifyContent(FlexAlign.SpaceAround)
        }
        Row() {
          //第一列
          Scroll(this.scrollerFirst) {
            Column() {
              Text(' ').height(this.customizePickerModel.textHeight)
              Text(' ').height(this.customizePickerModel.textHeight)
              if (this.customizePickerModel.isPicker) {
                ForEach(this.customizePickerModel.pickerData,
                  (item: CustomizePickerBean) => {
                    Text((item.firstName).toString())
                      .fontSize(this.customizePickerModel.fontSize)
                      .height(this.customizePickerModel.textHeight)
                      .fontColor(this.customizePickerModel.fontColor)
                  },
                  (item: CustomizePickerBean) => item.id.toString())
              } else {
                ForEach(this.customizePickerModel.firstArray,
                  (item: string) => {
                    Text(item.toString())
                      .fontSize(this.customizePickerModel.fontSize)
                      .height(this.customizePickerModel.textHeight)
                      .fontColor(this.customizePickerModel.fontColor)
                  },
                  (item: string) => item.toString())
              }
              Text(' ').height(this.customizePickerModel.textHeight)
              Text(' ').height(this.customizePickerModel.textHeight)
            }.width(this.customizePickerModel.columnWidth)
          }.scrollBar(BarState.Off)

          .onScrollEnd(() => {
            // 滑动过后，计算滑动距离，保持选择项在红色线之内
            this.scrollerFirst.scrollTo({
              xOffset: 0,
              yOffset: Math.floor(this.scrollerFirst.currentOffset()
                .yOffset / this.customizePickerModel.textHeight + this.halfMoveRatio) * this.customizePickerModel.textHeight,
              animation: { duration: 2000, curve: Curve.Ease }
            })
            this.scrollerFirstIndex = Math.floor(this.scrollerFirst.currentOffset()
              .yOffset / this.customizePickerModel.textHeight + this.halfMoveRatio)
            //将第二例位置重置
            this.scrollerSecond.scrollTo({
              xOffset: 0,
              yOffset: 0
            })
          })

          //是否第二列
          Scroll(this.scrollerSecond) {
            Column() {
              Text(' ').height(this.customizePickerModel.textHeight)
              Text(' ').height(this.customizePickerModel.textHeight)
              if (this.customizePickerModel.isPicker) {
                ForEach(this.customizePickerModel.pickerData[this.scrollerFirstIndex].secondAndThirdArray,
                  (item: SecondAndThirdArray) => {
                    Text((item.secondName).toString())
                      .fontSize(this.customizePickerModel.fontSize)
                      .height(this.customizePickerModel.textHeight)
                      .fontColor(this.customizePickerModel.fontColor)
                  },
                  (item: SecondAndThirdArray) => item.ids.toString())
              } else {
                ForEach(this.customizePickerModel.secondArray,
                  (item: string) => {
                    Text(item.toString())
                      .fontSize(this.customizePickerModel.fontSize)
                      .height(this.customizePickerModel.textHeight)
                      .fontColor(this.customizePickerModel.fontColor)
                  },
                  (item: string) => item.toString())
              }
              Text(' ').height(this.customizePickerModel.textHeight)
              Text(' ').height(this.customizePickerModel.textHeight)
            }.width(this.customizePickerModel.columnWidth)
          }.scrollBar(BarState.Off)
          .onScrollEnd(() => {
            // 滑动过后，计算滑动距离，保持选择项在红色线之内
            this.scrollerSecond.scrollTo({
              xOffset: 0,
              //如果移动的单位数超过字体高度的一半，则移动一个位置。否则回归原位
              yOffset: Math.floor(this.scrollerSecond.currentOffset()
                .yOffset / this.customizePickerModel.textHeight + this.halfMoveRatio) * this.customizePickerModel.textHeight,
              animation: { duration: 2000, curve: Curve.Ease }
            })
            this.scrollerSecondIndex = Math.floor(this.scrollerSecond.currentOffset()
              .yOffset / this.customizePickerModel.textHeight + this.halfMoveRatio)
            //将第三列位置重置
            this.scrollerThird.scrollTo({
              xOffset: 0,
              yOffset: 0
            })
          })

          // 是否第三列
          Scroll(this.scrollerThird) {
            Column() {
              Text(' ').height(this.customizePickerModel.textHeight)
              Text(' ').height(this.customizePickerModel.textHeight)
              if (this.customizePickerModel.isPicker) {
                ForEach(this.customizePickerModel.pickerData[this.scrollerFirstIndex].secondAndThirdArray[this.scrollerSecondIndex].thirdArray,
                  (item: string) => {
                    Text(item.toString())
                      .fontSize(this.customizePickerModel.fontSize)
                      .fontColor(this.customizePickerModel.fontColor)
                      .height(this.customizePickerModel.textHeight)
                  })
              } else {
                ForEach(this.customizePickerModel.thirdArray,
                  (itemString: number) => {
                    Text(`${itemString}`.toString())
                      .fontSize(this.customizePickerModel.fontSize)
                      .fontColor(this.customizePickerModel.fontColor)
                      .height(this.customizePickerModel.textHeight)
                  },
                  (val: string) => val.toString())
              }
              Text(' ').height(this.customizePickerModel.textHeight)
              Text(' ').height(this.customizePickerModel.textHeight)
            }.width(this.customizePickerModel.columnWidth)
          }.scrollBar(BarState.Off)
          .onScrollEnd(() => {
            this.scrollerThird.scrollTo({
              xOffset: 0,
              yOffset: Math.floor(this.scrollerThird.currentOffset()
                .yOffset / this.customizePickerModel.textHeight + this.halfMoveRatio) * this.customizePickerModel.textHeight,
              animation: { duration: 2000, curve: Curve.Ease }
            })
            this.scrollerThirdIndex = Math.floor(this.scrollerThird.currentOffset()
              .yOffset / this.customizePickerModel.textHeight + this.halfMoveRatio)
          })
        }.height(this.customizePickerModel.popupWindowHeight)
      }
    }.backgroundColor(Color.White)
  }
}

@Component
struct CustomizePickerComponent {
  @State model: CustomizePickerComponent.Model = new CustomizePickerComponent.Model();
  dialogController: CustomDialogController = new CustomDialogController({
    builder: CustomizeDialogController({
      cancel: this.onCancel,
      confirm: this.onAccept,
      customizePickerModel: this.model
    }),
    cancel: this.existApp,
    autoCancel: true,
    alignment: DialogAlignment.Bottom
  })
  // 城市
  onCancel() {
    console.info('Callback when the city first button is clicked')
  }

  onAccept() {
    console.info('Callback when the city second button is clicked')
  }

  existApp() {
    console.info('Click the callback in the blank area')
  }

  build() {
    //设置Stack的对齐方式为底部起始端对齐，Stack默认为居中对齐。设置Stack构造参数alignContent为Alignment.BottomStart。
    // 其中Alignment和FontWeight一样，都是框架提供的内置枚举类型
    Column() {
      Row() {
        Text('自定义选择器')
          .height(50)
          .fontSize(20)
          .fontColor(Color.Black)
          .backgroundColor(Color.Gray)
          .width("90%")
          .textAlign(TextAlign.Center)
          .onClick(() => {
            this.dialogController.open()
          })
      }.alignItems(VerticalAlign.Top).margin({ top: this.model.pickerSpace })
    }.alignItems(HorizontalAlign.Center).width("100%")
  }
}

namespace CustomizePickerComponent {
  export class Model {
    text: string = ''
    firstArray: any[] = []
    secondArray: any[] = []
    thirdArray: any[] = []
    dividerLineColor: Color = Color.Red;
    dividerLineStroke: number = 1
    thirdIsShow: boolean = false
    columnWidth: string = '50%'
    fontSize: number = 30
    fontColor: Color = Color.Black;
    titleFontSize: number = 20
    titleFontColor: Color = Color.Black;
    cancelButtonFont: string = "cancel"
    confirmButtonFont: string = "confirm"
    cancelButtonColor: Color = Color.Green;
    confirmButtonColor: Color = Color.Green;
    color: string = '';
    onclick: any = null;
    pickerSpace: number = 15
    buttonBackgroundColor: string = "#7CDCDC"
    dividerType?: DividerType = DividerType.FILL
    lineSpacingMultiplier: number = 0
    popupWindowHeight: number = 250
    textHeight: number = 50
    isPicker: boolean = true
    pickerData: CustomizePickerBean[] = []
    lineWidth: number = px2vp(deviceWidth * 0.25)

    setLineSpacingMultiplier(lineSpacingMultiplier: number): Model {
      if (lineSpacingMultiplier > 50) {
        lineSpacingMultiplier = 50
      }
      this.lineSpacingMultiplier = lineSpacingMultiplier
      this.textHeight = 50 + lineSpacingMultiplier
      this.popupWindowHeight = this.textHeight * 5
      return this;
    }

    setDividerType(dividerType: DividerType): Model {
      this.dividerType = dividerType
      return this;
    }

    setButtonBackgroundColor(buttonBackgroundColor: string): Model {
      this.buttonBackgroundColor = buttonBackgroundColor;
      return this;
    }

    setPickerSpace(pickerSpace: number): Model {
      this.pickerSpace = pickerSpace;
      return this;
    }

    constructor(firstArray?: any[]) {
      this.firstArray = firstArray
    }

    setDividerLineStroke(dividerLineStroke: number): Model {
      if (0 < dividerLineStroke && dividerLineStroke <= 5) {
        this.dividerLineStroke = dividerLineStroke;
      } else {
        dividerLineStroke = 2
      }
      return this;
    }

    setDividerLineColor(color: Color): Model {
      this.dividerLineColor = color;
      return this;
    }

    setThirdIsShow(bool: boolean): Model {
      this.thirdIsShow = bool;
      if (bool) { //根据区是否显示控制省、市的宽度
        this.columnWidth = '33%'
      } else {
        this.columnWidth = '50%'
      }
      return this;
    }

    setPicker(firstArray: any[], secondArray: any[], thirdArray: any[]): Model {
      this.isPicker = true
      this.firstArray = firstArray
      this.secondArray = secondArray
      this.thirdArray = thirdArray
      this.pickerData = initializeCustomizePickerDataOnStartup(firstArray, secondArray, thirdArray)
      return this;
    }

    setNPicker(firstArray: any[], secondArray: any[], thirdArray: any[]): Model {
      this.isPicker = false
      this.firstArray = firstArray
      this.secondArray = secondArray
      this.thirdArray = thirdArray
      return this;
    }

    setFontSize(fontSize: number): Model {
      this.fontSize = fontSize;
      return this;
    }

    setFontColor(fontColor: Color): Model {
      this.fontColor = fontColor;
      return this;
    }

    setTitleFontSize(titleFontSize: number): Model {
      this.titleFontSize = titleFontSize;
      return this;
    }

    setTitleFontColor(titleFontColor: Color): Model {
      this.titleFontColor = titleFontColor;
      return this;
    }

    setCancelButtonFont(cancelButtonFont: string): Model {
      this.cancelButtonFont = cancelButtonFont;
      return this;
    }

    setConfirmButtonFont(confirmButtonFont: string): Model {
      this.confirmButtonFont = confirmButtonFont;
      return this;
    }

    setCancelButtonColor(cancelButtonColor: Color): Model {
      this.cancelButtonColor = cancelButtonColor;
      return this;
    }

    setConfirmButtonColor(confirmButtonColor: Color): Model {
      this.confirmButtonColor = confirmButtonColor;
      return this;
    }

    withText(text: string): Model {
      this.text = text;
      return this;
    }

    withClick(callback: (event?: ClickEvent) => void): Model {
      this.onclick = callback;
      return this;
    }
  }
}

export default CustomizePickerComponent;