/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { initializeProvinceOnStartup, Province, CityAndArea } from './province'
import { DividerType, deviceWidth } from './Constant'

@CustomDialog
struct CustomDialogCityExample {
  private currentThis: any
  controller: CustomDialogController
  cancel: () => void
  confirm: () => void
  scrollerProvince: Scroller = new Scroller()
  scrollerCity: Scroller = new Scroller()
  scrollerArea: Scroller = new Scroller()
  private firstProvinceInsert: boolean = true
  private firstCityInsert: boolean = true
  private firstAreaInsert: boolean = true
  private lastProvincePosition: number
  private lastCityPosition: number
  private lastAreaPosition: number
  private buttonHeight: number = 50
  private array = [1, 2]
  private confirmValue: string
  @State provinceArray: Province[] = initializeProvinceOnStartup()
  @State scrollerProvinceIndex: number = 0
  @State scrollerCityIndex: number = 0
  @State scrollerAreaIndex: number = 0
  @State cityPickerModel: CityPickerComponent.Model = new CityPickerComponent.Model()

  public aboutToAppear() {
    // 获取初始化选择省的index
    this.provinceArray.forEach((val, idx, array) => {
      val.cityAndArea.forEach((valCity, idxCity, arrayCity) => {
        if (valCity.cityName == this.cityPickerModel.defaultSelection[1]) {
          this.scrollerProvinceIndex = idx
        }
      });
    });
  }

  build() {
    Column() {
      Flex({ justifyContent: FlexAlign.SpaceAround }) {
        Button(this.cityPickerModel.cancelButtonFont)
          .height(this.buttonHeight)
          .fontSize(this.cityPickerModel.titleFontSize)
          .backgroundColor(this.cityPickerModel.buttonBackgroundColor)
          .fontColor(this.cityPickerModel.cancelButtonColor)
          .margin({ left: 10 })
          .onClick(() => {
            this.controller.close()
            this.cancel()
          })
        Column() {
          Row() {
            Text("城市选择")
              .height(this.buttonHeight)
              .fontSize(this.cityPickerModel.titleFontSize)
              .fontColor(this.cityPickerModel.titleFontColor)
          }
        }

        Button(this.cityPickerModel.confirmButtonFont)
          .fontSize(this.cityPickerModel.titleFontSize)
          .backgroundColor(this.cityPickerModel.buttonBackgroundColor)
          .fontColor(this.cityPickerModel.confirmButtonColor)
          .onClick(() => {
            let provinceName = this.provinceArray[this.scrollerProvinceIndex].provinceName
            let cityName = this.provinceArray[this.scrollerProvinceIndex].cityAndArea[this.scrollerCityIndex].cityName
            if (!this.cityPickerModel.areaIsShow) {
              this.confirmValue = provinceName + '/' + cityName
            } else {
              let areaName = this.provinceArray[this.scrollerProvinceIndex].cityAndArea[this.scrollerCityIndex].area[this.scrollerAreaIndex]
              this.confirmValue = provinceName + '/' + cityName + '/' + areaName
            }
            this.controller.close()
            this.confirm()
          })
      }.backgroundColor("#7CDCDC").margin({ bottom: 0 })

      Stack({ alignContent: Alignment.TopStart }) {
        if (this.cityPickerModel.dividerType == DividerType.FILL) {
          Divider()
            .vertical(false)
            .strokeWidth(this.cityPickerModel.dividerLineStroke)
            .color(this.cityPickerModel.dividerLineColor)
            .lineCap(LineCapStyle.Round)
            .margin({ top: this.cityPickerModel.textHeight * 2 })
          Divider()
            .vertical(false)
            .strokeWidth(this.cityPickerModel.dividerLineStroke)
            .color(this.cityPickerModel.dividerLineColor)
            .lineCap(LineCapStyle.Round)
            .margin({ top: this.cityPickerModel.textHeight * 3 })
        } else if (this.cityPickerModel.dividerType == DividerType.CIRCLE) {
          Row({ space: 30 }) {
            Ellipse({ width: "40%", height: this.cityPickerModel.textHeight })
              .fillOpacity(0)
              .stroke(this.cityPickerModel.dividerLineColor)
              .strokeWidth(this.cityPickerModel.dividerLineStroke)
            Ellipse({ width: "40%", height: this.cityPickerModel.textHeight })
              .fillOpacity(0)
              .stroke(this.cityPickerModel.dividerLineColor)
              .strokeWidth(this.cityPickerModel.dividerLineStroke)
          }.margin({ top: this.cityPickerModel.textHeight * 2 })
          .justifyContent(FlexAlign.Center)
          .width("100%")

        } else if (this.cityPickerModel.dividerType == DividerType.WRAP) {
          Row() {
            ForEach(this.array,
              (index: number) => {
                Column() {
                  Divider()
                    .vertical(false)
                    .strokeWidth(this.cityPickerModel.dividerLineStroke)
                    .color(this.cityPickerModel.dividerLineColor)
                    .lineCap(LineCapStyle.Round)
                    .margin({ top: this.cityPickerModel.textHeight * 2 })
                  Divider()
                    .vertical(false)
                    .strokeWidth(this.cityPickerModel.dividerLineStroke)
                    .color(this.cityPickerModel.dividerLineColor)
                    .lineCap(LineCapStyle.Round)
                    .margin({ top: this.cityPickerModel.textHeight * 1 })
                }.width(this.cityPickerModel.columnLeftWidth)
              })
          }.justifyContent(FlexAlign.SpaceAround)
          .width("100%")
        }
        Row() {
          Scroll(this.scrollerProvince) {
            Column() {
              //  省
              Text(" ").fontSize(30).height(this.cityPickerModel.textHeight)
              Text(" ").fontSize(30).height(this.cityPickerModel.textHeight)
              ForEach(this.provinceArray,
                (provinceItem: Province) => {
                  Text(provinceItem.provinceName)
                    .fontSize(this.cityPickerModel.fontSize)
                    .fontColor(this.cityPickerModel.fontColor)
                    .height(this.cityPickerModel.textHeight)
                })
              Text(" ").fontSize(30).height(this.cityPickerModel.textHeight)
              Text(" ").fontSize(30).height(this.cityPickerModel.textHeight)
            }.width(this.cityPickerModel.columnWidth)
          }.scrollBar(BarState.Off)
          .onScroll((xOffset: number, yOffset: number) => {
            this.lastProvincePosition = yOffset == 0 ? this.lastProvincePosition : yOffset
            if (this.firstProvinceInsert) { //首次进入，用于初始化位置
              if (this.cityPickerModel.defaultSelection[0] != null) {
                this.provinceArray.forEach((val, idx, array) => {
                  if (this.cityPickerModel.defaultSelection[0] == val.provinceName) {
                    this.scrollerProvince.scrollTo({
                      xOffset: 0,
                      yOffset: idx * this.cityPickerModel.textHeight,
                      animation: { duration: 10, curve: Curve.Ease }
                    })
                  }
                });
              }
              this.firstProvinceInsert = false
            }
          })
          .onScrollEdge((side: Edge) => { // 滚动功能
            this.scrollerProvinceIndex = this.scrollerProvince.currentOffset().yOffset / this.cityPickerModel.textHeight
          })
          .onScrollEnd(() => {
            let scrollProvinceOffset = this.scrollerProvince.currentOffset().yOffset % this.cityPickerModel.textHeight
            if (scrollProvinceOffset > 1) {
              if (this.lastProvincePosition <= 0) { // 小于0，向下滑动，大于0是，是向上滑动
                // 获取选中的省
                this.scrollerProvince.scrollTo({ xOffset: 0, yOffset: this.scrollerProvince.currentOffset().yOffset
                - scrollProvinceOffset, animation: { duration: 2000, curve: Curve.Ease } })
              } else {
                this.scrollerProvince.scrollTo({
                  xOffset: 0,
                  yOffset: this.scrollerProvince.currentOffset().yOffset
                  + this.cityPickerModel.textHeight - scrollProvinceOffset,
                  animation: { duration: 2000, curve: Curve.Ease }
                })
                this.scrollerProvince.currentOffset().yOffset =
                this.scrollerProvince.currentOffset().yOffset + this.cityPickerModel.textHeight
              }
            }
            this.scrollerProvinceIndex = (this.scrollerProvince.currentOffset().yOffset - scrollProvinceOffset)
            / this.cityPickerModel.textHeight
          })

          Scroll(this.scrollerCity) {
            Column() {
              // 城市
              Text(' ').height(this.cityPickerModel.textHeight)
              Text(' ').height(this.cityPickerModel.textHeight)
              ForEach(this.provinceArray[this.scrollerProvinceIndex].cityAndArea,
                (val: CityAndArea) => {
                  if (val.cityName.length > 5) {
                    Text(val.cityName)
                      .fontSize(this.cityPickerModel.lengthOverFive_fontSize)
                      .fontColor(this.cityPickerModel.fontColor)
                      .height(this.cityPickerModel.textHeight)
                  } else {
                    Text(val.cityName)
                      .fontSize(this.cityPickerModel.fontSize)
                      .fontColor(this.cityPickerModel.fontColor)
                      .height(this.cityPickerModel.textHeight)
                  }
                },
                (val: CityAndArea) => val.id.toString())
              Text(' ').height(this.cityPickerModel.textHeight)
              Text(' ').height(this.cityPickerModel.textHeight)
            }.width(this.cityPickerModel.columnWidth)
          }.scrollBar(BarState.Off)
          .onScroll((xOffset: number, yOffset: number) => {
            this.lastCityPosition = yOffset == 0 ? this.lastCityPosition : yOffset
            if (this.firstCityInsert) { //首次进入，用于初始化位置
              if (this.cityPickerModel.defaultSelection[1] != null) {
                this.provinceArray[this.scrollerProvinceIndex].cityAndArea.forEach((val, idx, array) => {
                  if (this.cityPickerModel.defaultSelection[1] == val.cityName) {
                    this.scrollerCity.scrollTo({
                      xOffset: 0,
                      yOffset: idx * this.cityPickerModel.textHeight,
                      animation: { duration: 10, curve: Curve.Ease }
                    })
                  }
                });
              }
              this.firstCityInsert = false
            }
          })
          .onScrollEnd(() => {
            // 滑动过后，计算滑动距离，保持选择项在红色线之内
            let scrollCityOffset = this.scrollerCity.currentOffset().yOffset % this.cityPickerModel.textHeight
            if (scrollCityOffset > 1) {
              if (this.lastCityPosition < 0) { // 小于0，向下滑动，大于0是，是向上滑动
                // 获取选中的市
                this.scrollerCity.scrollTo({ xOffset: 0, yOffset: this.scrollerCity.currentOffset().yOffset
                - scrollCityOffset, animation: { duration: 2000, curve: Curve.Ease } })
                this.scrollerCityIndex = (
                  this.scrollerCity.currentOffset()
                    .yOffset - scrollCityOffset) / this.cityPickerModel.textHeight
              } else {
                this.scrollerCity.scrollTo({
                  xOffset: 0,
                  yOffset: this.scrollerCity.currentOffset().yOffset
                  + this.cityPickerModel.textHeight - scrollCityOffset,
                  animation: { duration: 2000, curve: Curve.Ease }
                })
                this.scrollerCityIndex = (this.scrollerCity.currentOffset().yOffset - scrollCityOffset)
                / this.cityPickerModel.textHeight + 1
              }
            }
          })
          // 是否显示区
          if (this.cityPickerModel.areaIsShow) {
            Scroll(this.scrollerArea) {
              Column() {
                // 城市
                Text(' ').height(this.cityPickerModel.textHeight)
                Text(' ').height(this.cityPickerModel.textHeight)
                ForEach(this.provinceArray[this.scrollerProvinceIndex].cityAndArea[this.scrollerCityIndex].area,
                  (area: string) => {
                    Text(area)
                      .fontSize(this.cityPickerModel.fontSize)
                      .fontColor(this.cityPickerModel.fontColor)
                      .height(this.cityPickerModel.textHeight)
                  },
                  (val: string) => val.toString())
                Text(' ').height(this.cityPickerModel.textHeight)
                Text(' ').height(this.cityPickerModel.textHeight)
              }.width(this.cityPickerModel.columnWidth)
            }.scrollBar(BarState.Off)
            .onScroll((xOffset: number, yOffset: number) => {
              this.lastAreaPosition = yOffset == 0 ? this.lastAreaPosition : yOffset
              if (this.firstAreaInsert) { //首次进入，用于初始化位置
                if (this.cityPickerModel.defaultSelection[2] != null) {
                  this.provinceArray[this.scrollerProvinceIndex].cityAndArea[this.scrollerCityIndex].area.forEach((val, idx, array) => {
                    if (this.cityPickerModel.defaultSelection[2] == val) {
                      this.scrollerArea.scrollTo({
                        xOffset: 0,
                        yOffset: idx * this.cityPickerModel.textHeight,
                        animation: { duration: 10, curve: Curve.Ease }
                      })
                    }
                  });
                }
                this.firstAreaInsert = false
              }
            })
            .onScrollEnd(() => {
              let scrollCityOffset = this.scrollerArea.currentOffset().yOffset % this.cityPickerModel.textHeight
              if (scrollCityOffset > 1) {
                if (this.lastAreaPosition <= 0) { // 小于0，向下滑动，大于0是，是向上滑动
                  // 获取选中的省
                  this.scrollerArea.scrollTo({ xOffset: 0, yOffset: this.scrollerArea.currentOffset().yOffset
                  - scrollCityOffset, animation: { duration: 2000, curve: Curve.Ease } })
                  this.scrollerAreaIndex = (this.scrollerArea.currentOffset().yOffset - scrollCityOffset)
                  / this.cityPickerModel.textHeight
                } else {
                  this.scrollerArea.scrollTo({
                    xOffset: 0,
                    yOffset: this.scrollerArea.currentOffset().yOffset
                    + this.cityPickerModel.textHeight - scrollCityOffset,
                    animation: { duration: 2000, curve: Curve.Ease }
                  })
                  this.scrollerAreaIndex = (this.scrollerArea.currentOffset().yOffset - scrollCityOffset)
                  / this.cityPickerModel.textHeight + 1
                }
              }
            })
          }
        }.height(this.cityPickerModel.popupWindowHeight).alignItems(VerticalAlign.Bottom)

      }
    }.backgroundColor(Color.White)
  }
}

@Component
struct CityPickerComponent {
  private currentThis = this
  @State selectValue: string = ""
  private confirmValue: string = ""
  @State model: CityPickerComponent.Model = new CityPickerComponent.Model();
  dialogCityController: CustomDialogController = new CustomDialogController({
    builder: CustomDialogCityExample({
      currentThis: this.currentThis,
      cancel: this.onCityCancel,
      confirm: this.onCityAccept,
      cityPickerModel: this.model,
      confirmValue: this.confirmValue
    }),
    cancel: this.existApp,
    autoCancel: true,
    alignment: DialogAlignment.Bottom
    //offset:{dx: 0, dy:150}
  })
  // 城市
  onCityCancel() {
    console.info('Callback when the city second button is clicked')
  }

  onCityAccept() {
    this.currentThis.selectValue = this.confirmValue
    // console.info(JSON.stringify(this.cityPickerModel.textHeight) + "onCityAccept:" )
    //this.cityPickerModel.callback(this.that.selectValue);
  }

  existApp() {
    console.info('Click the callback in the blank area')
  }

  build() {
    //设置Stack的对齐方式为底部起始端对齐，Stack默认为居中对齐。设置Stack构造参数alignContent为Alignment.BottomStart。
    // 其中Alignment和FontWeight一样，都是框架提供的内置枚举类型
    Column() {
      Row() {
        Text(this.selectValue.length > 0 ? this.selectValue : '城市选择器')
          .height(50)
          .fontSize(20)
          .fontColor(Color.Black)
          .backgroundColor(Color.Gray)
          .width("90%")
          .textAlign(TextAlign.Center)
          .onClick(() => {
            console.info("onCityAccept111111:" + this.model.confirmButtonFont)
            this.dialogCityController.open()
          })
      }.alignItems(VerticalAlign.Top).margin({ top: this.model.pickerSpace })
    }.alignItems(HorizontalAlign.Center).width("100%")

  }
}

namespace CityPickerComponent {
  export class Model {
    text: string = ''
    dividerLineColor: Color = Color.Red;
    dividerLineStroke: number = 1
    areaIsShow: boolean = false
    columnWidth: string = '50%'
    fontSize: number = 30
    fontColor: Color = Color.Black;
    titleFontSize: number = 30
    titleFontColor: Color = Color.Black;
    cancelButtonFont: string = "cancel"
    confirmButtonFont: string = "confirm"
    cancelButtonColor: Color = Color.Green;
    confirmButtonColor: Color = Color.Green;
    lengthOverFive_fontSize: number = 30
    color: string = '';
    onclick: any = null;
    pickerSpace: number = 15
    buttonBackgroundColor: string = "#7CDCDC"
    defaultSelection: any[] = []
    dividerType?: DividerType = DividerType.FILL
    lineSpacingMultiplier: number = 0
    popupWindowHeight: number = 250
    textHeight: number = 50
    columnLeftWidth: any = px2vp(deviceWidth * 0.3)
    callback: (a) => any

    setCallback(callback: (a) => any): Model {
      console.warn("setCallback" + callback)
      this.callback = callback
      return this
    }

    setLineSpacingMultiplier(lineSpacingMultiplier: number): Model {
      if (lineSpacingMultiplier > 50) {
        lineSpacingMultiplier = 50
      }
      this.lineSpacingMultiplier = lineSpacingMultiplier
      this.textHeight = 50 + lineSpacingMultiplier
      this.popupWindowHeight = this.textHeight * 5
      return this;
    }

    setDividerType(dividerType: DividerType): Model {
      this.dividerType = dividerType
      return this;
    }

    setDefaultSelection(defaultSelection: any[]): Model {
      this.defaultSelection = defaultSelection;
      return this;
    }

    setButtonBackgroundColor(buttonBackgroundColor: string): Model {
      this.buttonBackgroundColor = buttonBackgroundColor;
      return this;
    }

    setLengthOverFive_fontSize(fontSize: number): Model {
      this.lengthOverFive_fontSize = fontSize;
      return this;
    }

    setPickerSpace(pickerSpace: number): Model {
      this.pickerSpace = pickerSpace;
      return this;
    }

    constructor(dividerLineStroke?: number) {

      //      this.dividerLineStroke = dividerLineStroke;
      console.log("in Builder constructor")
    }

    setDividerLineStroke(dividerLineStroke: number): Model {
      if (0 < dividerLineStroke && dividerLineStroke <= 5) {
        this.dividerLineStroke = dividerLineStroke;
      } else {
        dividerLineStroke = 2
      }
      return this;
    }

    setDividerLineColor(color: Color): Model {
      this.dividerLineColor = color;
      return this;
    }

    setAreaIsShow(bool: boolean): Model {
      this.areaIsShow = bool;
      if (bool) { //根据区是否显示控制省、市的宽度
        this.columnWidth = '33%'
      } else {
        this.columnWidth = '50%'
      }
      return this;
    }

    setFontSize(fontSize: number): Model {
      this.fontSize = fontSize;
      return this;
    }

    setFontColor(fontColor: Color): Model {
      this.fontColor = fontColor;
      return this;
    }

    setTitleFontSize(titleFontSize: number): Model {
      this.titleFontSize = titleFontSize;
      return this;
    }

    setTitleFontColor(titleFontColor: Color): Model {
      this.titleFontColor = titleFontColor;
      return this;
    }

    setCancelButtonFont(cancelButtonFont: string): Model {
      this.cancelButtonFont = cancelButtonFont;
      return this;
    }

    setConfirmButtonFont(confirmButtonFont: string): Model {
      this.confirmButtonFont = confirmButtonFont;
      return this;
    }

    setCancelButtonColor(cancelButtonColor: Color): Model {
      this.cancelButtonColor = cancelButtonColor;
      return this;
    }

    setConfirmButtonColor(confirmButtonColor: Color): Model {
      this.confirmButtonColor = confirmButtonColor;
      return this;
    }

    withText(text: string): Model {
      this.text = text;
      return this;
    }

    withClick(callback: (event?: ClickEvent) => void): Model {
      this.onclick = callback;
      return this;
    }
  }
}

export default CityPickerComponent;