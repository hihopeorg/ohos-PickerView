/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { initializeProvinceOnStartup, Province, CityAndArea } from './province'
import router from '@ohos.router'
import {showToast, setTimeOut} from './showtoast'
import {DividerType} from './Constant'
import { initializeSolarCalendar, SolarCalendar , MonthAndDay} from './solarcalendar'

@Component
struct ShowTimePickerViewLib {
  private showTimePickerModel: ShowTimePickerViewLib.Model = new ShowTimePickerViewLib.Model()
  private solarYearsArray: SolarCalendar[]
    = initializeSolarCalendar(this.showTimePickerModel.yearRangeStart,this.showTimePickerModel.yearRangeEnd)

  @State imageState : boolean = false
  @State YearsArray : any[] = this.solarYearsArray
  @State solarLatestYearIndex : number = 0
  @State solarLatestMonthIndex : number = 0
  @State solarLatestDayIndex : number = 0
  @State showTimePickerView : boolean = false
  private firstYearInsert: boolean = true
  private firstMonthInsert : boolean = true
  private firstDayInsert: boolean = true
  private lastYearPosition: number
  private lastMonthPosition: number
  private lastDayPosition: number
  private caculatorYear: number = 1
  private caculatorMonth: number = 1
  private caculatorDay: number = 1
  private buttonHeight : number = 50
  private array = [1,2,3]
  scrollerYear: Scroller = new Scroller()
  scrollerMonth: Scroller = new Scroller()
  scrollerDay: Scroller = new Scroller()
  @State showText: string = "显示时间选择器"
  build() {
    Column() {
      Row() {
        Text(this.showText)
          .height(this.buttonHeight)
          .fontSize(this.showTimePickerModel.titleFontSize)
          .fontColor(this.showTimePickerModel.titleFontColor)
          .backgroundColor(Color.Gray)
          .width("90%")
          .textAlign(TextAlign.Center)
          .onClick(() => {
            this.showTimePickerView = !this.showTimePickerView
            if (this.showTimePickerView) {//再次打开，将index置为0
              this.solarLatestYearIndex = 0
              this.solarLatestMonthIndex = 0
              this.solarLatestDayIndex = 0
            }
          })
      }.alignItems(VerticalAlign.Top).margin({top: 10})
      if (this.showTimePickerView) {
        Row(){
          Flex({ justifyContent: FlexAlign.SpaceBetween }) {
            Button(this.showTimePickerModel.cancelButtonFont).height(this.buttonHeight)
              .fontSize(this.showTimePickerModel.titleFontSize)
              .backgroundColor(this.showTimePickerModel.buttonBackgroundColor)
              .fontColor(this.showTimePickerModel.cancelButtonColor)
              .margin({left:10})
              .onClick(() => {
                router.back()
              })
            Button(this.showTimePickerModel.confirmButtonFont)
              .fontSize(this.showTimePickerModel.titleFontSize)
              .backgroundColor(this.showTimePickerModel.buttonBackgroundColor)
              .fontColor(this.showTimePickerModel.confirmButtonColor)
              .onClick(() => {
                let selectYear = this.YearsArray[this.solarLatestYearIndex].year
                let selectMonth = this.YearsArray[this.solarLatestYearIndex].monthAndDay[this.solarLatestMonthIndex].month
                let daysArray = this.YearsArray[this.solarLatestYearIndex].monthAndDay[this.solarLatestMonthIndex].days
                if (daysArray.length < (this.solarLatestDayIndex + 1)) {
                  this.solarLatestDayIndex = daysArray.length - 1
                }
                let selectDay = daysArray[this.solarLatestDayIndex]
                this.showText = selectYear + "-" + selectMonth + "-" + selectDay
              })
          }.backgroundColor("#7CDCDC").margin({ bottom: 0 })
        }.margin({ top: 10})
        Stack({ alignContent: Alignment.TopStart }) {
          if (this.showTimePickerModel.dividerType == DividerType.FILL) {
            Divider()
              .vertical(false)
              .strokeWidth(this.showTimePickerModel.dividerLineStroke)
              .color(this.showTimePickerModel.dividerLineColor)
              .lineCap(LineCapStyle.Round)
              .margin({ top: this.showTimePickerModel.textHeight * 2})
            Divider()
              .vertical(false)
              .strokeWidth(this.showTimePickerModel.dividerLineStroke)
              .color(this.showTimePickerModel.dividerLineColor)
              .lineCap(LineCapStyle.Round)
              .margin({ top: this.showTimePickerModel.textHeight * 3})
          }else if(this.showTimePickerModel.dividerType == DividerType.CIRCLE) {
            Circle({ width: "30%", height: this.showTimePickerModel.textHeight})
              .fillOpacity(0).stroke(this.showTimePickerModel.dividerLineColor)
              .strokeWidth(this.showTimePickerModel.dividerLineStroke)
              .margin({ left: this.showTimePickerModel.columnLeftWidth[0], top: this.showTimePickerModel.textHeight * 2})
            Circle({width: "30%", height: this.showTimePickerModel.textHeight})
              .fillOpacity(0)
              .stroke(this.showTimePickerModel.dividerLineColor)
              .strokeWidth(this.showTimePickerModel.dividerLineStroke)
              .margin({ left: this.showTimePickerModel.columnLeftWidth[1], top: this.showTimePickerModel.textHeight * 2})
            Circle({ width: "30%", height: this.showTimePickerModel.textHeight})
              .fillOpacity(0)
              .stroke(this.showTimePickerModel.dividerLineColor)
              .strokeWidth(this.showTimePickerModel.dividerLineStroke)
              .margin({ left: this.showTimePickerModel.columnLeftWidth[2], top: this.showTimePickerModel.textHeight * 2})
          } else if(this.showTimePickerModel.dividerType == DividerType.WRAP) {
            ForEach(this.array,
              (index: number) => {
                Divider().vertical(false)
                  .strokeWidth(this.showTimePickerModel.dividerLineStroke)
                  .color(this.showTimePickerModel.dividerLineColor)
                  .lineCap(LineCapStyle.Round)
                  .margin({left: this.showTimePickerModel.columnLeftWidth[index], top: this.showTimePickerModel.textHeight * 2})
                  .width("30%")
                Divider().vertical(false)
                  .strokeWidth(this.showTimePickerModel.dividerLineStroke)
                  .color(this.showTimePickerModel.dividerLineColor)
                  .lineCap(LineCapStyle.Round)
                  .margin({left: this.showTimePickerModel.columnLeftWidth[index], top: this.showTimePickerModel.textHeight * 3})
                  .width("30%")
              },
              (index: number) => index.toString())
          }
          Row() {
            Scroll(this.scrollerYear) {
              Column() {
                // 年
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                ForEach(this.YearsArray,
                  (solarYearItem: SolarCalendar) => {
                    Text((solarYearItem.year).toString())
                      .fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                  },
                  (day: number) => day.toString())
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
              }.width("32%")
            }.scrollBar(BarState.Off)
            .onScroll((xOffset: number, yOffset: number) => { // 计算偏移量
              this.lastYearPosition = yOffset == 0 ?  this.lastYearPosition : yOffset
              if (this.firstYearInsert) {//首次进入，用于初始化位置
                if (this.showTimePickerModel.defaultSelection[0] != null) {
                  this.YearsArray.forEach((val, idx, array) => {
                    if (this.showTimePickerModel.defaultSelection[0] == val.year) {
                      this.scrollerYear.scrollTo({ xOffset: 0, yOffset: idx * this.showTimePickerModel.textHeight, animation: {duration: 1, curve: Curve.Ease}})
                      this.solarLatestYearIndex = idx
                    }
                  });
                }
                this.firstYearInsert = false
              }
            })
            .onScrollEnd(() => { // 保持选择项据中
              // 滑动过后，计算滑动距离，保持选择项在红色线之内
              let scrollYearOffset = this.scrollerYear.currentOffset().yOffset % this.showTimePickerModel.textHeight
              if (scrollYearOffset > 1) {
                if (this.lastYearPosition <= 0) {// 小于0，向下滑动，大于0是，是向上滑动
                  // 获取选中的年
                  this.solarLatestYearIndex = (this.scrollerYear.currentOffset().yOffset - scrollYearOffset) / this.showTimePickerModel.textHeight
                  this.scrollerYear.scrollTo({ xOffset: 0, yOffset: this.scrollerYear.currentOffset().yOffset
                  - scrollYearOffset, animation: {duration: 2000, curve: Curve.Ease}})
                } else {
                  this.solarLatestYearIndex = (this.scrollerYear.currentOffset().yOffset - scrollYearOffset)
                  / this.showTimePickerModel.textHeight + 1
                  this.scrollerYear.scrollTo({ xOffset: 0, yOffset: this.scrollerYear.currentOffset().yOffset
                  + this.showTimePickerModel.textHeight - scrollYearOffset, animation: {duration: 2000, curve: Curve.Ease}})
                }
              }
            })
            .onScrollEdge((side: Edge) => { // 滚动功能
              this.solarLatestYearIndex = this.scrollerYear.currentOffset().yOffset / this.showTimePickerModel.textHeight
            })
            Column() {
              Divider().vertical(true).color(Color.Gray).height(this.showTimePickerModel.textHeight * 5).strokeWidth(0.5)
            }.width("2%")
            // 月
            Scroll(this.scrollerMonth) {
              Column() {
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                //  月
                ForEach(this.YearsArray[this.solarLatestYearIndex].monthAndDay,
                  (monthAndDay: MonthAndDay) => {
                    Text(monthAndDay.month.toString())
                      .fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                  },
                  (day: number) => day.toString())
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
              }.width('32%')
            }.scrollBar(BarState.Off)
            .onScroll((xOffset: number, yOffset: number) => {
              this.lastMonthPosition = yOffset == 0 ?  this.lastMonthPosition : yOffset
              if (this.firstMonthInsert) {//首次进入，用于初始化位置
                if (this.showTimePickerModel.defaultSelection[1] != null) {
                  this.YearsArray[this.solarLatestYearIndex].monthAndDay.forEach((val, idx, array) => {
                    if (this.showTimePickerModel.defaultSelection[1] == val.month) {
                      this.scrollerMonth.scrollTo({ xOffset: 0, yOffset: idx * this.showTimePickerModel.textHeight, animation: {duration: 1, curve: Curve.Ease}})
                      //为初始化值设置index
                      this.solarLatestMonthIndex = idx
                    }
                  });
                }
                this.firstMonthInsert = false
              }
            })
            .onScrollEnd(() => {
              let scrollMonthOffset = this.scrollerMonth.currentOffset().yOffset % this.showTimePickerModel.textHeight
              // 滑动过后，计算滑动距离，保持选择项在红色线之内
              if (scrollMonthOffset > 1) {
                if (this.lastMonthPosition <= 0) {// 小于0，向下滑动，大于0是，是向上滑动
                  this.solarLatestMonthIndex = (this.scrollerMonth.currentOffset().yOffset - scrollMonthOffset)
                  / this.showTimePickerModel.textHeight
                  this.scrollerMonth.scrollTo({ xOffset: 0, yOffset: this.scrollerMonth.currentOffset().yOffset
                  - scrollMonthOffset, animation: {duration: 2000, curve: Curve.Ease}})
                } else {
                  this.solarLatestMonthIndex = (this.scrollerMonth.currentOffset().yOffset - scrollMonthOffset)
                  / this.showTimePickerModel.textHeight + 1
                  //计算选择的 月
                  this.scrollerMonth.scrollTo({ xOffset: 0, yOffset: this.scrollerMonth.currentOffset().yOffset
                  + this.showTimePickerModel.textHeight - scrollMonthOffset, animation: {duration: 2000, curve: Curve.Ease}})
                }
                // 处理 所选月份的天数小于之前INDEX
                let dayLength = this.YearsArray[this.solarLatestYearIndex].monthAndDay[this.solarLatestMonthIndex].days.length - 1
                if (this.solarLatestDayIndex > dayLength) {
                  this.solarLatestDayIndex = dayLength;
                }
              }
            })
            .onScrollEdge((side: Edge) => { // 滚动功能
              this.solarLatestMonthIndex = this.scrollerMonth.currentOffset().yOffset / this.showTimePickerModel.textHeight
            })
            Column() {
              Divider().vertical(true).color(Color.Gray).height(this.showTimePickerModel.textHeight * 5).strokeWidth(0.5)
            }.width("2%")
            // 日
            Scroll(this.scrollerDay) {
              Column() {
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                // 阳历 日
                ForEach(this.YearsArray[this.solarLatestYearIndex].monthAndDay[this.solarLatestMonthIndex].days,
                  (day: any) => {
                    Text(day.toString())
                      .fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                  },
                  (day: number) => day.toString())
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
                Text(" ").fontSize(this.showTimePickerModel.fontSize).height(this.showTimePickerModel.textHeight)
              }.width('32%')
            }.scrollBar(BarState.Off)
            .onScroll((xOffset: number, yOffset: number) => {
              this.lastDayPosition = yOffset == 0 ?  this.lastMonthPosition : yOffset
              if (this.firstDayInsert) {//首次进入，用于初始化位置
                if (this.showTimePickerModel.defaultSelection[2] != null) {
                  this.YearsArray[this.solarLatestYearIndex].monthAndDay[this.solarLatestMonthIndex].days.forEach((val, idx, array) => {
                    if (this.showTimePickerModel.defaultSelection[2] == val) {
                      this.scrollerDay.scrollTo({ xOffset: 0, yOffset: idx * this.showTimePickerModel.textHeight, animation: {duration: 1, curve: Curve.Ease}})
                      this.solarLatestDayIndex = idx
                    }
                  });
                }
                this.firstDayInsert = false
              }
            })
            .onScrollEnd(() => {
              let scrollDayOffset = this.scrollerDay.currentOffset().yOffset % this.showTimePickerModel.textHeight
              // 滑动过后，计算滑动距离，保持选择项在红色线之内
              // 小于0，向下滑动，大于0是，是向上滑动
              if (scrollDayOffset > 1) {
                if (this.lastDayPosition <= 0) {
                  this.scrollerDay.scrollTo({ xOffset: 0, yOffset: this.scrollerDay.currentOffset().yOffset
                  - scrollDayOffset, animation: {duration: 2000, curve: Curve.Ease}})
                  this.solarLatestDayIndex = (this.scrollerDay.currentOffset().yOffset - scrollDayOffset) / this.showTimePickerModel.textHeight
                }else{
                  this.scrollerDay.scrollTo({ xOffset: 0, yOffset: this.scrollerDay.currentOffset().yOffset + this.showTimePickerModel.textHeight
                  -  scrollDayOffset, animation: {duration: 2000, curve: Curve.Ease}})
                  this.solarLatestDayIndex = (this.scrollerDay.currentOffset().yOffset - scrollDayOffset) / this.showTimePickerModel.textHeight + 1
                }
              }
            })
            .onScrollEdge((side: Edge) => { // 滚动功能
              this.solarLatestDayIndex = this.scrollerDay.currentOffset().yOffset / this.showTimePickerModel.textHeight
            })
          }.height(this.showTimePickerModel.popupWindowHeight)
        }.width('100%').height('100%').align(Alignment.Center)
      }
    }.alignItems(HorizontalAlign.Center).width("100%")
  }
}

namespace ShowTimePickerViewLib {
  export class Model {
    text : string = ''
    dividerLineColor: Color = Color.Red;
    dividerLineStroke: number = 1
    fontSize: number = 30
    fontColor: Color = Color.Black;
    titleFontSize: number = 20
    titleFontColor: Color = Color.Black;
    cancelButtonFont: string = "cancel"
    confirmButtonFont: string = "canfirm"
    cancelButtonColor: Color = Color.Green;
    confirmButtonColor: Color = Color.Green;
    color: string = '';
    onclick: any = null;
    yearRangeStart : number = 2010
    yearRangeEnd : number = 2030
    pickerSpace : number = 15
    buttonBackgroundColor : string = "#7CDCDC"
    defaultSelection: any[] = []
    dividerType?: DividerType = DividerType.FILL
    lineSpacingMultiplier: number = 0
    popupWindowHeight: number = 250
    textHeight: number = 50
    columnLeftWidth: string[] = ["1%","35%", "69%"]

    setLineSpacingMultiplier(lineSpacingMultiplier: number) :Model {
      if (lineSpacingMultiplier > 50) {
        lineSpacingMultiplier = 50
      }
      this.lineSpacingMultiplier = lineSpacingMultiplier
      this.textHeight = 50 + lineSpacingMultiplier
      this.popupWindowHeight = this.textHeight * 5
      return this;
    }
    setDividerType(dividerType: DividerType) :Model {
      this.dividerType = dividerType
      return this;
    }
    setDefaultSelection(defaultSelection : any[]): Model {
      this.defaultSelection = defaultSelection;
      return this;
    }
    setButtonBackgroundColor(buttonBackgroundColor : string): Model {
      this.buttonBackgroundColor = buttonBackgroundColor;
      return this;
    }
    setPickerSpace(pickerSpace : number): Model {
      this.pickerSpace = pickerSpace;
      return this;
    }
    setYearRangeStart(yearRangeStart : number): Model {
      this.yearRangeStart = yearRangeStart;
      return this;
    }

    setYearRangeEnd(yearRangeEnd : number): Model {
      this.yearRangeEnd = yearRangeEnd;
      return this;
    }
    setDividerLineStroke(dividerLineStroke : number): Model {
      if (0 < dividerLineStroke && dividerLineStroke <= 5) {
        this.dividerLineStroke = dividerLineStroke;
      } else {
        dividerLineStroke = 2
      }
      return this;
    }

    setDividerLineColor(color: Color): Model {
      this.dividerLineColor = color;
      return this;
    }

    setFontSize(fontSize: number): Model {
      this.fontSize = fontSize;
      return this;
    }

    setFontColor(fontColor: Color): Model {
      this.fontColor = fontColor;
      return this;
    }

    setTitleFontSize(titleFontSize: number): Model {
      this.titleFontSize = titleFontSize;
      return this;
    }

    setTitleFontColor(titleFontColor: Color): Model {
      this.titleFontColor = titleFontColor;
      return this;
    }

    setCancelButtonFont(cancelButtonFont: string): Model {
      this.cancelButtonFont = cancelButtonFont;
      return this;
    }

    setConfirmButtonFont(confirmButtonFont: string): Model {
      this.confirmButtonFont = confirmButtonFont;
      return this;
    }

    setCancelButtonColor(cancelButtonColor: Color): Model {
      this.cancelButtonColor = cancelButtonColor;
      return this;
    }

    setConfirmButtonColor(confirmButtonColor: Color): Model {
      this.confirmButtonColor = confirmButtonColor;
      return this;
    }

    withText(text:string): Model {
      this.text = text;
      return this;
    }

    withClick(callback: (event?: ClickEvent) => void): Model {
      this.onclick = callback;
      return this;
    }
  }
}

export default ShowTimePickerViewLib;