/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@ohos.router'
import {CircleDividerViewLib} from '@ohos/pickerview'

@Entry
@Component
struct CircleDividerPickerView {
  private model : CircleDividerViewLib.Model = new CircleDividerViewLib.Model()
  aboutToAppear() {
      this.model.setDividerLineStroke(<number>router.getParams()['dividerLineStroke'])
      .setDividerLineColor(<Color>router.getParams()['dividerLineColor'])
      .setFirstArray(<any>router.getParams()['firstArray'])
      .setFontSize(<number>router.getParams()['fontSize'])
      .setFontColor(<Color>router.getParams()['fontColor'])
      .setConfirmButtonFont(<string>router.getParams()['confirmButtonFont'])
      .setCancelButtonFont(<string>router.getParams()['cancelButtonFont'])
      .setCancelButtonColor(<Color>router.getParams()['cancelButtonColor'])
      .setConfirmButtonColor(<Color>router.getParams()['confirmButtonColor'])
      .setTitleFontSize(<number>router.getParams()['titleFontSize'])
      .setTitleFontColor(<Color>router.getParams()['titleFontColor'])
      .setPickerSpace(<number>router.getParams()['pickerSpace'])
      .setButtonBackgroundColor(<string>router.getParams()['buttonBackgroundColor'])
      .setDefaultSelection(<any>router.getParams()['defaultSelection'])
  }
  build() {
    Column() {
      CircleDividerViewLib({model: this.model})
    }
  }
}